/* globals OblakoEditor: false */
(function ($) {

	'use strict';

	var BlockUtil = {
		blocktags: ['blockquote', 'h3', 'ul', 'ol'],
		listtag: 'li',

		appendToBlock: function (nodes, tag) {
			this.removeFromBlock(nodes);
			$(nodes).wrapAll($('<' + tag + '>'));
		},

		appendToListBlock: function (nodes, tag) {
			this.removeFromBlock(nodes);
			var jnodes = $(nodes);
			jnodes.wrapAll('<' + tag + '>');
			jnodes.wrap('<' + this.listtag + '>');
		},

		removeFromBlock: function (nodes) {
			var localNodes = Array.prototype.slice.call(nodes).reverse(),
				followingSiblings;

			for (var i = 0; i < localNodes.length; i++) {
				var jnode = $(localNodes[i]),
					blockNode = jnode.parent();
				if (this.blocktags.indexOf(blockNode.prop('tagName').toLowerCase()) >= 0) {
					followingSiblings = jnode.nextAll();
					jnode.detach().insertAfter(blockNode);
					followingSiblings.detach().insertAfter(jnode).wrapAll('<' + blockNode.prop('tagName') + '>');
					if (blockNode.text().length === 0) {
						blockNode.remove();
					}
				} else if (this.listtag === blockNode.prop('tagName').toLowerCase()) {
					blockNode = blockNode.parent();
					if (this.blocktags.indexOf(blockNode.prop('tagName').toLowerCase()) >= 0) {
						var p = jnode.parent();
						jnode.detach().insertAfter(blockNode);
						followingSiblings = p.nextAll();
						p.remove();
						followingSiblings.detach().insertAfter(jnode).wrapAll('<' + blockNode.prop('tagName') + '>');
						if (blockNode.text().length === 0) {
							blockNode.remove();
						}
					}
				}
			}
		},

		getSelectedNodes: function (document, editorElement) {
			var range = document.getSelection().getRangeAt(0),
				startNode = range.startContainer,
				endNode = range.endContainer,
				ans = [];

			if (startNode.nodeType === Node.TEXT_NODE) {
				startNode = startNode.parentNode;
			}
			if (endNode.nodeType === Node.TEXT_NODE) {
				endNode = endNode.parentNode;
			}

			// Selection either the same text node, or the whole editor
			if (startNode === endNode) {
				var descendantParagraphs = this.getAllParagraphDescendants(startNode);
				if (descendantParagraphs.length > 0) {
					return descendantParagraphs;
				} else {
					while (startNode.localName !== 'p' && startNode !== editorElement && startNode.parentNode !== null) {
						startNode = startNode.parentNode;
					}
					if (startNode.localName === 'p') {
						return [startNode];
					}
					return [];
				}
			}

			while (startNode.parentNode !== range.commonAncestorContainer) {
				startNode = startNode.parentNode;
			}
			while (endNode.parentNode !== range.commonAncestorContainer) {
				endNode = endNode.parentNode;
			}
			while (startNode !== endNode) {
				ans = ans.concat(this.getAllParagraphDescendants(startNode));
				startNode = startNode.nextSibling;
			}

			return ans.concat(this.getAllParagraphDescendants(endNode));
		},

		getAllParagraphDescendants: function (node) {
			if (node.localName === 'p') {
				return [node];
			}
			var nodes = [];
			if (node.hasChildNodes()) {
				var children = node.childNodes;
				for (var i = 0; i < children.length; i++) {
					nodes = nodes.concat(this.getAllParagraphDescendants(children[i]));
				}
			}
			return nodes;
		},

		getBlockName: function (node, editorElement) {
			while (node !== editorElement) {
				if (this.blocktags.indexOf(node.localName.toLowerCase()) >= 0) {
					return node.localName;
				}
				node = node.parentNode;
			}
			return null;
		}
	};

	OblakoEditor.blockUtil = BlockUtil;

})(jQuery);