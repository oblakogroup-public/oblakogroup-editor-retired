/* globals OblakoEditor: false */
// Default image dragging using medium-insert's file uploading
(function ($) {

	'use strict';

	var CLASS_DRAG_OVER = 'medium-editor-dragover';

	function clearClassNames(element) {
		var editable = MediumEditor.util.getContainerEditorElement(element),
			existing = Array.prototype.slice.call(editable.parentElement.querySelectorAll('.' + CLASS_DRAG_OVER));

		existing.forEach(function (el) {
			el.classList.remove(CLASS_DRAG_OVER);
		});
	}

	var ImageDragging = MediumEditor.Extension.extend({
		name: 'image-dragging',

		allowedTypes: ['image'],

		init: function () {
			MediumEditor.Extension.prototype.init.apply(this, arguments);

			this.subscribe('editableDrag', this.handleDrag.bind(this));
			this.subscribe('editableDrop', this.handleDrop.bind(this));
		},

		handleDrag: function (event) {
			event.preventDefault();
			event.dataTransfer.dropEffect = 'copy';

			var target = event.target.classList ? event.target : event.target.parentElement;

			// Ensure the class gets removed from anything that had it before
			clearClassNames(target);

			if (event.type === 'dragover') {
				target.classList.add(CLASS_DRAG_OVER);
			}
		},

		handleDrop: function (event, editorElement) {
			// Prevent file from opening in the current window
			event.preventDefault();
			event.stopPropagation();

			this.checkTargetBlock(event, editorElement);

			this.uploadFile(event.dataTransfer.files, editorElement);

			// Make sure we remove our class from everything
			clearClassNames(event.target);
		},

		checkTargetBlock: function (event, editorElement) {
			var target = event.target,
				p;
			if (target === editorElement) {
				p = this.getSuitableTarget();
				target.insertBefore(p, target.lastChild);
				// Insert before '.medium-insert-buttons'
				MediumEditor.selection.moveCursor(this.document, p);
				return target;
			}
			while (target.parentNode !== editorElement) {
				target = target.parentNode;
			}
			// If target is paragraph with only <br> child then proceed
			if (target.tagName.toLowerCase() === 'p' && target.childNodes.length === 1 &&
				target.firstChild.nodeType === Node.ELEMENT_NODE &&
				target.firstChild.tagName.toLowerCase() === 'br') {
				return target;
			} else { // We create paragraph with only <br> child and make it target
				p = this.getSuitableTarget();
				editorElement.insertBefore(p, target.nextSibling);
				MediumEditor.selection.moveCursor(this.document, p);
				return p;
			}

		},

		getSuitableTarget: function () {
			var p = this.document.createElement('p'),
				br = this.document.createElement('br');
			p.appendChild(br);

			// Remove class 'medium-insert-active' from all elements which have it
			var elements = this.document.getElementsByClassName('medium-insert-active');
			for (var i = 0; i < elements.length; i++) {
				elements[i].className = elements[i].className.replace(/(?:^|\s)medium-insert-active(?!\S)/ , '');
			}

			p.className += 'medium-insert-active';
			return p;
		},

		isAllowedFile: function (file) {
			return this.allowedTypes.some(function (fileType) {
				return !!file.type.match(fileType);
			});
		},

		// Uploading file.
		// Code based on medium-insert's Images.prototype.add()
		uploadFile: function (fileList, editorElement) {
			var mediumInsertImages = $(editorElement).data('plugin_mediumInsertImages'),
			$file = $(mediumInsertImages.templates['src/js/templates/images-fileupload.hbs']()),
			fileUploadOptions = {
				dataType: 'json',
				add: function (e, data) {
					$.proxy(mediumInsertImages, 'uploadAdd', e, data)();
				},
				done: function (e, data) {
					$.proxy(mediumInsertImages, 'uploadDone', e, data)();
				}
			};

			// Only add progress callbacks for browsers that support XHR2,
			// and test for XHR2 per:
			// http://stackoverflow.com/questions/6767887/
			// what-is-the-best-way-to-check-for-xhr2-file-upload-support
			if (new XMLHttpRequest().upload) {
				fileUploadOptions.progress = function (e, data) {
					$.proxy(mediumInsertImages, 'uploadProgress', e, data)();
				};

				fileUploadOptions.progressall = function (e, data) {
					$.proxy(mediumInsertImages, 'uploadProgressall', e, data)();
					$('.medium-insert-active').removeClass('medium-insert-active');
				};
			}

			$file.fileupload($.extend(true, {}, mediumInsertImages.options.fileUploadOptions, fileUploadOptions));
			$file.fileupload('add', { files: fileList });
		}
	});

	OblakoEditor.extensions.imageDragging = ImageDragging;

}(jQuery));
