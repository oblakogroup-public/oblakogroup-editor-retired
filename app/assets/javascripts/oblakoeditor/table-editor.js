/* globals OblakoEditor: false */
// Table manipulation methods from MediumTable Editor pulled out into separate plugin
(function ($) {

	'use strict';

	var TableEditor = MediumEditor.Extension.extend({
		name: 'table-editor',

		labels: {
			row: 'Строка:',
			addRowBefore: 'Добавить строку сверху',
			addRowAfter: 'Добавить строку снизу',
			remRow: 'Удалить строку',
			column: 'Колонка:',
			addColumnBefore: 'Добавить колонку слева',
			addColumnAfter: 'Добавить колонку справа',
			remColumn: 'Удалить колонку',
			remTable: 'Удалить таблицу'
		},

		toolbarOffsets: {
			left: 0,
			top: -10
		},

		init: function () {
			this.shown = false;

			this.tableEditor = new TEditor({
				plugin: this,
				doc: this.document,
				labels: this.labels,
				offsets: this.toolbarOffsets
			});
			this.getEditorOption('elementsContainer').appendChild(this.tableEditor.getElement());

			this.getEditorElements().forEach(function (element) {
				this.on(element, 'mouseup', this.handleMouse.bind(this, element));
			}, this);

			this.base.subscribe('editableKeyup', this.handleKeyboard.bind(this));
			this.base.subscribe('showToolbar', this.hideToolbar.bind(this));

			this.base.subscribe('blur', function (event) {
				// If a click is on toolbar element, don't hide it
				if (!this.tableEditor._toolbar.contains(event.target)) {
					this.hideToolbar();
				}
			}.bind(this));
		},

		handleMouse: function (editorElement, event) {
			var element = event.target;
			this.handleChange(element, editorElement);
		},

		handleKeyboard: function (event, editorElement) {
			if (this.shown === true) {
				var range = MediumEditor.selection.getSelectionRange(this.document),
					element = MediumEditor.selection.getSelectedParentElement(range);
				this.handleChange(element, editorElement);
			}
		},

		handleChange: function (element, editorElement) {
			if (this.isInsideTable(element, editorElement)) {
				if (this.shown === false) {
					this.showToolbar();
				} else {
					this.updateToolbarPosition();
				}
			} else {
				if (this.shown === true) {
					this.hideToolbar();
				}
			}
		},

		isInsideTable: function (element, editorElement) {
			if (!editorElement.contains(element)) {
				return false;
			}
			while (element !== editorElement) {
				if (element.nodeName.toLowerCase() === 'table') {
					return true;
				}
				element = element.parentNode;
			}
			return false;
		},

		hideToolbar: function () {
			this.shown = false;
			this.tableEditor.hide();
		},

		showToolbar: function () {
			this.shown = true;
			var range = MediumEditor.selection.getSelectionRange(this.document);
			this.tableEditor.show(MediumEditor.selection.getSelectedParentElement(range));
		},

		updateToolbarPosition: function () {
			var range = MediumEditor.selection.getSelectionRange(this.document);
			this.tableEditor.updatePosition(MediumEditor.selection.getSelectedParentElement(range));
		}

	});

	OblakoEditor.extensions.tableEditor = TableEditor;

	function TEditor(options) {
		return this.init(options);
	}

	TEditor.prototype = {
		init: function (options) {
			this.options = options;
			this._plugin = this.options.plugin;
			this._doc = this.options.doc;
			this._toolbar = this._doc.createElement('div');
			this._toolbar.className = 'medium-editor-table-editor-toolbar';
			this._target = null;

			var spanRow = this._doc.createElement('span');
			spanRow.innerHTML = this.options.labels.row;
			this._toolbar.appendChild(spanRow);

			var addRowBefore = this._doc.createElement('button');
			addRowBefore.title = this.options.labels.addRowBefore;
			addRowBefore.innerHTML = '<i class="fa fa-long-arrow-up"></i>';
			addRowBefore.addEventListener('mouseup', this.addRow.bind(this, true));
			this._toolbar.appendChild(addRowBefore);

			var addRowAfter = this._doc.createElement('button');
			addRowAfter.title = this.options.labels.addRowAfter;
			addRowAfter.innerHTML = '<i class="fa fa-long-arrow-down"></i>';
			addRowAfter.addEventListener('mouseup', this.addRow.bind(this, false));
			this._toolbar.appendChild(addRowAfter);

			var remRow = this._doc.createElement('button');
			remRow.title = this.options.labels.remRow;
			remRow.innerHTML = '<i class="fa fa-close"></i>';
			remRow.addEventListener('mouseup', this.removeRow.bind(this));
			this._toolbar.appendChild(remRow);

			var spanCol = this._doc.createElement('span');
			spanCol.innerHTML = this.options.labels.column;
			this._toolbar.appendChild(spanCol);

			var addColumnBefore = this._doc.createElement('button');
			addColumnBefore.title = this.options.labels.addColumnBefore;
			addColumnBefore.innerHTML = '<i class="fa fa-long-arrow-left"></i>';
			addColumnBefore.addEventListener('mouseup', this.addColumn.bind(this, true));
			this._toolbar.appendChild(addColumnBefore);

			var addColumnAfter = this._doc.createElement('button');
			addColumnAfter.title = this.options.labels.addColumnAfter;
			addColumnAfter.innerHTML = '<i class="fa fa-long-arrow-right"></i>';
			addColumnAfter.addEventListener('mouseup', this.addColumn.bind(this, false));
			this._toolbar.appendChild(addColumnAfter);

			var remColumn = this._doc.createElement('button');
			remColumn.title = this.options.labels.remColumn;
			remColumn.innerHTML = '<i class="fa fa-close"></i>';
			remColumn.addEventListener('mouseup', this.removeColumn.bind(this));
			this._toolbar.appendChild(remColumn);

			var remTable = this._doc.createElement('button');
			remTable.title = this.options.labels.remTable;
			remTable.innerHTML = '<i class="fa fa-trash-o"></i>';
			remTable.addEventListener('mouseup', this.removeTable.bind(this));
			this._toolbar.appendChild(remTable);
		},

		getElement: function () {
			return this._toolbar;
		},

		hide: function () {
			this._toolbar.style.display = '';
		},

		show: function (target) {
			this._target = target;
			this._toolbar.style.display = 'block';

			this.updatePosition();
		},

		updatePosition: function (target) {
			if (target !== undefined) {
				this._target = target;
			}
			var coords = $(this._target).offset();

			this._toolbar.style.left = (coords.left + this.options.offsets.left) + 'px';
			this._toolbar.style.top = (coords.top - this._toolbar.offsetHeight + this.options.offsets.top) + 'px';
		},

		getEditorElement: function (el) {
			if (el === undefined) {
				el = this._target;
			}
			while (el !== null && !el.classList.contains('oblakoeditor')) {
				el = el.parentNode;
			}
			return el;
		},

		findCell: function () {
			var thisCell = this._target;
			while (thisCell.nodeName.toLowerCase() !== 'td' && thisCell.nodeName.toLowerCase() !== 'th') {
				thisCell = thisCell.parentNode;
			}
			return thisCell;
		},

		addRow: function (before, event) {
			event.preventDefault();
			event.stopPropagation();
			var thisTR = this.findCell().parentNode,
				tbody = thisTR.parentNode,
				editorElement = this.getEditorElement(tbody),
				tr = this._doc.createElement('tr'),
				td;

			for (var i = 0; i < thisTR.childNodes.length; i++) {
				td = this._doc.createElement('td');
				td.appendChild(this._doc.createElement('br'));
				tr.appendChild(td);
			}
			if (before !== true && thisTR.nextSibling) {
				tbody.insertBefore(tr, thisTR.nextSibling);
			} else if (before === true) {
				tbody.insertBefore(tr, thisTR);
			} else {
				tbody.appendChild(tr);
			}
			this.updatePosition();
			this._plugin.base.events.triggerCustomEvent('editableInput', event, editorElement);
		},

		removeRow: function (event) {
			event.preventDefault();
			event.stopPropagation();
			var tr = this.findCell().parentNode,
				editorElement = this.getEditorElement(tr);
			tr.parentNode.removeChild(tr);
			this._plugin.hideToolbar();
			this.updatePosition();
			this._plugin.base.events.triggerCustomEvent('editableInput', event, editorElement);
		},

		addColumn: function (before, event) {
			event.preventDefault();
			event.stopPropagation();
			var cell = this.findCell(),
				tr = cell.parentNode,
				tbody = tr.parentNode,
				cellIndex = Array.prototype.indexOf.call(tr.childNodes, cell),
				editorElement = this.getEditorElement(tbody),
				td;
			for (var i = 0; i < tbody.childNodes.length; i++) {
				td = this._doc.createElement('td');
				td.appendChild(this._doc.createElement('br'));
				if (before === true) {
					tbody.childNodes[i].insertBefore(td, tbody.childNodes[i].childNodes[cellIndex]);
				} else if (tbody.childNodes[i].childNodes[cellIndex].nextSibling) {
					tbody.childNodes[i].insertBefore(td, tbody.childNodes[i].childNodes[cellIndex].nextSibling);
				} else {
					tbody.childNodes[i].appendChild(td);
				}
			}
			this.updatePosition();
			this._plugin.base.events.triggerCustomEvent('editableInput', event, editorElement);
		},

		removeColumn: function (event) {
			event.preventDefault();
			event.stopPropagation();
			var cell = this.findCell(),
				tr = cell.parentNode,
				tbody = tr.parentNode,
				cellIndex = Array.prototype.indexOf.call(tr.childNodes, cell),
				rows = tbody.childNodes.length,
				editorElement = this.getEditorElement(tbody);

			for (var i = 0; i < rows; i++) {
				tbody.childNodes[i].removeChild(tbody.childNodes[i].childNodes[cellIndex]);
			}
			this._plugin.hideToolbar();
			this.updatePosition();
			this._plugin.base.events.triggerCustomEvent('editableInput', event, editorElement);
		},

		removeTable: function (event) {
			event.preventDefault();
			event.stopPropagation();
			var table = this.findCell().parentNode.parentNode.parentNode,
				p = this._doc.createElement('p'),
				mediumInsert = $(this.getEditorElement()).data('plugin_mediumInsert'),
				editorElement = this.getEditorElement(table);

			p.appendChild(this._doc.createElement('br'));
			table.parentNode.replaceChild(p, table);
			MediumEditor.selection.moveCursor(this._doc, p);
			this._plugin.hideToolbar();
			this._plugin.base.events.triggerCustomEvent('editableInput', event, editorElement);

			if (mediumInsert !== undefined) {
				mediumInsert.toggleButtons(event);
			}
		}
	};

})(jQuery);