module Oblakoeditor
	class Settings
		attr_accessor :core, :toolbar, :anchorPreview, :placeholder, :anchorForm, :paste,
			:keyboardCommands, :autoLink, :imageDragging, :fixes, :tables, :insert

		def initialize
			@core = Core.new

			@toolbar = Toolbar.new
			@toolbar.enabled = true
			# @toolbar.buttons = ['bold', 'italic', 'anchor', 'custom-header', 'custom-quote', 'custom-ul', 'custom-ol', { 'name' => 'justifyLeft', 'style' => { 'prop' => 'text-align', 'value' => 'left|start' } }, 'justifyCenter']
			@toolbar.buttons = ['bold', 'italic', 'anchor', 'h3', 'quote', 'ul', 'ol', { 'name' => 'justifyLeft', 'style' => { 'prop' => 'text-align', 'value' => 'left|start' } }, 'justifyCenter']
			
			@anchorPreview = AnchorPreview.new
			@anchorPreview.enabled = true
			
			@placeholder = Placeholder.new
			@placeholder.enabled = true
			@placeholder.text = 'Введите текст'

			@anchorForm = AnchorForm.new
			@anchorForm.linkValidation = true
			@anchorForm.placeholderText = 'Введите адрес ссылки'

			@paste = Paste.new
			@paste.enabled = true
			@paste.forcePlainText = false
			@paste.cleanPastedHTML = true
			@paste.cleanAttrs = ['dir', 'id', 'class', 'style', 'offline', 'onabort', 'onafterprint', 'onbeforeonload', 'onbeforeprint', 'onblur', 'oncanplay', 'oncanplaythrough', 'onchange', 'onclick', 'oncontextmenu', 'ondblclick', 'ondrag', 'ondragend', 'ondragenter', 'ondragleave', 'ondragover', 'ondragstart', 'ondrop', 'ondurationchange', 'onemptied', 'onended', 'onerror', 'onfocus', 'onformchange', 'onforminput', 'onhaschange', 'oninput', 'oninvalid', 'onkeydown', 'onkeypress', 'onkeyup', 'onload', 'onloadeddata', 'onloadedmetadata', 'onloadstart', 'onmessage', 'onmousedown', 'onmousemove', 'onmouseout', 'onmouseover', 'onmouseup', 'onmousewheel', 'onoffline', 'onoine', 'ononline', 'onpagehide', 'onpageshow', 'onpause', 'onplay', 'onplaying', 'onpopstate', 'onprogress', 'onratechange', 'onreadystatechange', 'onredo', 'onresize', 'onscroll', 'onseeked', 'onseeking', 'onselect', 'onstalled', 'onstorage', 'onsubmit', 'onsuspend', 'ontimeupdate', 'onundo', 'onunload', 'onvolumechange', 'onwaiting']
			@paste.cleanTags = ['iframe', 'meta', 'script', 'img']

			@keyboardCommands = KeyboardCommands.new
			@keyboardCommands.enabled = true

			@autoLink = AutoLink.new
			@autoLink.enabled = true
			
			@imageDragging = ImageDragging.new
			@imageDragging.enabled = true
			@imageDragging.useInsertPlugin = true

			@fixes = Fixes.new
			@fixes.enabled = true

			@tables = Tables.new
			@tables.enabled = true
			@tables.useInsertPlugin = true
			@tables.useEditor = true
			@tables.arrowKeys = true
			set_table_toolbar_buttons

			@insert = Insert.new
			@insert.enabled = true
			@insert.images = Insert::Images.new
			@insert.images.enabled = true
			@insert.images.captionPlaceholder = 'Введите подпись к картинке (опционально)'
			@insert.embeds = Insert::Embeds.new
			@insert.embeds.enabled = false
			@insert.razdatkaNote = Insert::RazdatkaNote.new
			@insert.razdatkaNote.enabled = false
		end

		def Settings.load_yaml file
			temp_settings = Settings.new
			temp_settings.load_yaml file
			return temp_settings
		end

		def load_yaml file
			options = YAML.load_file(file)
			load_yaml_section options, "core", %w(activeButtonClass allowMultiParagraphSelection buttonLabels contentWindow delay disableReturn disableDoubleReturn disableExtraSpaces disableEditing elementsContainer ownerDocument spellcheck targetBlank)
			load_yaml_section options, "toolbar", %w(enabled allowMultiParagraphSelection buttons diffLeft diffTop standardizeSelectionStart firstButtonClass lastButtonClass static align sticky updateOnEmptySelection)
			load_yaml_section options, "anchorPreview",  %w(enabled hideDelay previewValueSelector showWhenToolbarIsVisible)
			load_yaml_section options, "placeholder",  %w(enabled text hideOnClick)
			load_yaml_section options, "anchorForm",  %w(customClassOption customClassOptionText linkValidation placeholderText targetCheckbox targetCheckboxText)
			load_yaml_section options, "paste",  %w(forcePlainText cleanPastedHTML cleanReplacements cleanAttrs cleanTags)
			load_yaml_section options, "keyboardCommands",  %w(enabled commands)
			load_yaml_section options, "autoLink",  %w(enabled)
			load_yaml_section options, "imageDragging",  %w(enabled useInsertPlugin)
			load_yaml_section options, "fixes",  %w(enabled)
			load_yaml_section options, "tables",  %w(enabled rows columns useInsertPlugin useEditor arrowKeys)

			set_table_toolbar_buttons

			unless options["insert"].nil?
				@insert.enabled = options["insert"]["enabled"] unless options["insert"]["enabled"].nil?
				@insert.razdatkaNote.enabled = options["insert"]["razdatkaNote"]["enabled"] unless options["insert"]["razdatkaNote"].nil? || options["insert"]["razdatkaNote"]["enabled"].nil?
				@insert.embeds.enabled = options["insert"]["embeds"]["enabled"] unless options["insert"]["embeds"].nil? || options["insert"]["embeds"]["enabled"].nil?
				
				unless options["insert"]["images"].nil?
					attrs = %w(enabled label uploadScript deleteScript deleteMethod preview captions captionPlaceholder autoGrid formDatad fileUploadOptions fileDeleteOptions messages uploadCompleted)
					attrs.each { |a| @insert.images.send("#{a}=", options["insert"]["images"][a]) unless options["insert"]["images"][a].nil? }
				end
			end
		end

		private def set_table_toolbar_buttons
			@toolbar.buttons << 'table' if @tables.enabled && !@toolbar.buttons.include?('table')
			@toolbar.delete('table') if !@tables.enabled && @toolbar.buttons.include?('table')
		end

		# options - hash with new options
		# section - string with name of section
		# attrs - array with strings of names of attributes
		private def load_yaml_section options, section, attrs
			unless options[section].nil?
				attrs.each { |a| self.send(section).send("#{a}=", options[section][a]) unless options[section][a].nil? }
			end
		end

		class Core
			attr_accessor :activeButtonClass, :allowMultiParagraphSelection, :buttonLabels, :contentWindow,
				:delay, :disableReturn, :disableDoubleReturn, :disableExtraSpaces, :disableEditing,
				:elementsContainer, :ownerDocument, :spellcheck, :targetBlank
		end
		class Toolbar
			attr_accessor :enabled, :allowMultiParagraphSelection, :buttons, :diffLeft, :diffTop, :standardizeSelectionStart,
				:firstButtonClass, :lastButtonClass, :static, :align, :sticky, :updateOnEmptySelection
		end
		class AnchorPreview
			attr_accessor :enabled, :hideDelay, :previewValueSelector, :showWhenToolbarIsVisible
		end
		class Placeholder
			attr_accessor :enabled, :text, :hideOnClick
		end
		class AnchorForm
			attr_accessor :customClassOption, :customClassOptionText, :linkValidation,
				:placeholderText, :targetCheckbox, :targetCheckboxText
		end
		class Paste
			attr_accessor :enabled, :forcePlainText, :cleanPastedHTML, :cleanReplacements, :cleanAttrs, :cleanTags
		end
		class KeyboardCommands
			attr_accessor :enabled, :commands
		end
		class AutoLink
			attr_accessor :enabled
		end
		class ImageDragging
			attr_accessor :enabled, :useInsertPlugin
		end
		class Fixes
			attr_accessor :enabled
		end
		class Tables
			attr_accessor :enabled, :rows, :columns, :useInsertPlugin, :useEditor, :arrowKeys
		end
		class Insert
			attr_accessor :enabled, :images, :embeds, :razdatkaNote
			class Images
				attr_accessor :enabled, :label, :deleteScript, :deleteMethod, :preview,
					:captions, :captionPlaceholder, :autoGrid, :fileUploadOptions,
					:fileDeleteOptions, :messages, :uploadCompleted
			end
			class Embeds
				attr_accessor :enabled
			end
			class RazdatkaNote
				attr_accessor :enabled
			end
		end
	end

	mattr_accessor :settings
	@@settings = Settings.new
	@@settings.load_yaml 'config/oblakoeditor.yml' if File.exists? 'config/oblakoeditor.yml'
	def use_default_settings
		@@settings = Settings.new
	end
	module_function :use_default_settings
end