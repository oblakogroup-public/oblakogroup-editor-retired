$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "oblakoeditor/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "oblakoeditor"
  s.version     = Oblakoeditor::VERSION
  s.authors     = ["Tsyren Balzhanov"]
  s.email       = ["t.balzhanov@oblakogroup.ru"]
  s.homepage    = "https://bitbucket.org/tbalzhanovoblakogroup/oblakoeditor"
  s.summary     = "WYSIWYG editor based on MediumEditor"
  s.description = ""
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib,vendor}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc"]
  s.test_files = Dir["test/**/*"]

  s.add_dependency "jquery-rails", ">= 4"

end
