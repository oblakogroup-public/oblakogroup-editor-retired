//= require medium-editor/medium-editor
//= require medium-editor/medium-editor-tables
//= require medium-editor/dependencies/handlebars-v4.0.5
//= require medium-editor/dependencies/jquery-sortable
//= require medium-editor/dependencies/jquery.ui.widget
//= require medium-editor/dependencies/jquery.iframe-transport
//= require medium-editor/dependencies/jquery.fileupload
//= require medium-editor/medium-editor-insert-plugin